﻿namespace Library.Extensions
{
    public static class StringExtension
    {
        public static decimal ToDecimal(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return decimal.Parse(value);
            }

            return 0;
        }
    }
}
