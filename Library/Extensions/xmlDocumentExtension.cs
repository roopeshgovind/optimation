﻿using System.Xml;

namespace Library.Extensions
{
    public static class XmlDocumentExtension
    {
        public static string GetInnerText(this XmlDocument document, string xpath)
        {
            var node = document.SelectSingleNode(xpath);
            if (node != null)
            {
                return node.InnerText;
            }
            return string.Empty;
        }        
    }
}
