﻿using System;

namespace Library.Helpers
{
    public static class GstHelper
    {
        public static decimal CalculateGst(decimal total)
        {
            return Math.Round((total * 3) / 23, 2);
        }
    }
}
