﻿using System.Collections.Generic;
using System.Xml;

namespace Library.Helpers
{
    public class TextHelper
    {
        private const string _startTag = "<expense>";
        private const string _endTag = "</expense>";
        private const string _totalTag = "<total>";
        private string _input;

        #region Public Methods

        public TextHelper(string input)
        {
            _input = input;
        }
        public List<string> ValidateInput()
        {
            var errors = new List<string>();
            if (string.IsNullOrEmpty(_input))
            {
                errors.Add("Invalid input.");
                return errors;
            }

            if (_input.IndexOf(_endTag) == -1)
                errors.Add("Invalid xml, the end tag is missing.");

            if (_input.IndexOf(_totalTag) == -1)
                errors.Add("Invalid xml, total is missing.");

            return errors;
        }


        public XmlDocument Transform()
        {
            var extractedText = Extract(_input);
            if (!string.IsNullOrEmpty(extractedText))
            {
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(extractedText);
                return xml;
            }

            return null;
        }

        #endregion

        #region Private Methods

        private string Extract(string input)
        {
            var startIndex = input.IndexOf(_startTag);
            var endIndex = input.IndexOf(_endTag);
            var tagLength = _endTag.Length;

            if (endIndex > startIndex)
            {
                return input.Substring(startIndex, (endIndex - startIndex) + tagLength);
            }

            return null;
        }

        #endregion
    }
}
