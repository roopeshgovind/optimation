﻿using System.Linq;
using System.Xml;
using Library.Extensions;
using Library.Helpers;
using Library.Interfaces;
using Library.Model;


namespace Library.Implementations
{
    public class MessageService : IMessageService
    {
        public MessageResult Transform(string input)
        {
            var textHelper = new TextHelper(input);
            var result = new MessageResult
            {
                Errors = textHelper.ValidateInput()
            };

            if (result.Errors.Any())
            {
                return result;
            }

            var xmlDocument = textHelper.Transform();
            if (xmlDocument != null)
            {
                result = Map(xmlDocument);
                result.Gst = GstHelper.CalculateGst(result.Total);
            }

            return result;
        }


        #region Private Methods

        private MessageResult Map(XmlDocument document)
        {
            return new MessageResult()
            {
                CostCentre = document.GetInnerText("/expense/cost_centre"),
                PaymentMethod = document.GetInnerText("/expense/payment_method"),
                Total = document.GetInnerText("/expense/total").ToDecimal()
            };
        }

        #endregion
    }
}
