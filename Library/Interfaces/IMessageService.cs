﻿using System;
using System.Collections.Generic;
using System.Text;
using Library.Model;

namespace Library.Interfaces
{
    public interface IMessageService
    {
        MessageResult Transform(string input);
    }
}
