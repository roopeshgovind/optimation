﻿using System;
using System.Collections.Generic;

namespace Library.Model
{
    public class MessageResult
    {
        private string _costCentre;
        public string CostCentre
        {
            get
            {
                if (string.IsNullOrEmpty(_costCentre))
                {
                    _costCentre = "UNKNOWN";
                }

                return _costCentre;
            }
            set
            {
                _costCentre = value;
            }
        }
        public string PaymentMethod { get; set; }
        public decimal Total { get; set; }
        public decimal Gst { get; set; }
        public decimal TotalExcludingGst
        {
            get
            {
                return Math.Round(Total - Gst, 2);
            }
        }

        public List<string> Errors { get; set; } = new List<string>();
    }
}
