﻿using System;
using System.Collections.Generic;
using System.Text;
using Library.Helpers;

namespace Optimation
{
    public class Processor
    {        
        public void Run()
        {
            var helper = new TextHelper();
            var xml = helper.Transform(GetInput());
            var total = decimal.Parse(xml.SelectSingleNode("/expense/total").InnerText);
            var gstTotal = total * (decimal)1.15;
        }


        private string GetInput()
        {
            return @"Hi Yvaine,
                    Please create an expense claim for the below. Relevant details are marked up as requested…
                    <expense>
                    <cost_centre>DEV002</cost_centre>
                    <total>1024.01</total>
                    <payment_method>personal card</payment_method>
                    </expense>
                    From: Ivan Castle Sent: Friday, 16 February 2018 10:32 AM
                    To: Antoine Lloyd <Antoine.Lloyd@example.com>
                    Subject: test
                    Hi Antoine,
                    Please create a reservation at the <vendor>Viaduct Steakhouse</vendor> our
                    <description>development team’s project end celebration dinner</description> on <date>Tuesday
                    27 April 2017</date>. We expect to arrive around 7.15pm. Approximately 12 people but I’ll
                    confirm exact numbers closer to the day.
                    Regards,
                    Ivan";
        }
    }
}
