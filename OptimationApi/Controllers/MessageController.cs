﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Library.Interfaces;


namespace OptimationApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IMessageService _service;

        public MessageController(IMessageService service)
        {
            _service = service;
        }

        [HttpGet]
        public ActionResult<string> Get(string input)
        {
            if (string.IsNullOrEmpty(input))
                return BadRequest();

            var result = _service.Transform(input);
            if (result.Errors.Any())
                return NotFound(result);

            return Ok(result);
        }
    }
}