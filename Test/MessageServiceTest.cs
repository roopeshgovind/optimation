using System.Collections.Generic;
using System.Linq;
using Xunit;
using Library.Implementations;
using Library.Interfaces;


namespace Test
{
    public class MessageServiceTest
    {

        private readonly IMessageService _service = new MessageService();

        [Theory]
        [MemberData(nameof(GetValidTestData))]
        public void ValidTest(string input, decimal total, decimal gst, decimal totalExcludingGst, string costCentre)
        {
            var result = _service.Transform(input);

            Assert.True(!result.Errors.Any());
            Assert.Equal(total, result.Total);
            Assert.Equal(gst, result.Gst);
            Assert.Equal(totalExcludingGst, result.TotalExcludingGst);
            Assert.Equal(costCentre, result.CostCentre);
        }

        [Theory]
        [MemberData(nameof(GetInValidTestData))]
        public void InValidTest(string input, List<string> errors)
        {
            var result = _service.Transform(input);

            Assert.True(result.Errors.Any());
            Assert.Equal(errors, result.Errors);
        }

        #region Data Models
        

        public static IEnumerable<object[]> GetValidTestData()
        {
            yield return new object[] { GetInput1(), 1024.01, 133.57, 890.44, "DEV002" };
            yield return new object[] { GetInput2(), 115.00, 15.00, 100.00, "DEV002" };
            yield return new object[] { GetInput3(), 1024.01, 133.57, 890.44, "UNKNOWN" };
        }

        public static IEnumerable<object[]> GetInValidTestData()
        {
            yield return new object[] { "", new List<string>() { "Invalid input." } };
            yield return new object[] { GetInput4(), new List<string>() { "Invalid xml, the end tag is missing." } };
            yield return new object[] { GetInput5(), new List<string>() { "Invalid xml, total is missing." } };
        }

        
        private static string GetInput1()
        {
            return @"Hi Yvaine,
                Please create an expense claim for the below. Relevant details are marked up as requested�
                <expense>
                <cost_centre>DEV002</cost_centre>
                <total>1024.01</total>
                <payment_method>personal card</payment_method>
                </expense>
                From: Ivan Castle Sent: Friday, 16 February 2018 10:32 AM
                To: Antoine Lloyd <Antoine.Lloyd@example.com>
                Subject: test
                Hi Antoine,
                Please create a reservation at the <vendor>Viaduct Steakhouse</vendor> our
                <description>development team�s project end celebration dinner</description> on <date>Tuesday
                27 April 2017</date>. We expect to arrive around 7.15pm. Approximately 12 people but I�ll
                confirm exact numbers closer to the day.
                Regards,
                Ivan";
        }
        private static string GetInput2()
        {
            return @"Hi Yvaine,
                Please create an expense claim for the below. Relevant details are marked up as requested�
                <expense>
                <cost_centre>DEV002</cost_centre>
                <total>115.00</total>
                <payment_method>personal card</payment_method>
                </expense>
                From: Ivan Castle Sent: Friday, 16 February 2018 10:32 AM
                To: Antoine Lloyd <Antoine.Lloyd@example.com>
                Subject: test
                Hi Antoine,
                Please create a reservation at the <vendor>Viaduct Steakhouse</vendor> our
                <description>development team�s project end celebration dinner</description> on <date>Tuesday
                27 April 2017</date>. We expect to arrive around 7.15pm. Approximately 12 people but I�ll
                confirm exact numbers closer to the day.
                Regards,
                Ivan";
        }
        private static string GetInput3()
        {
            return @"Hi Yvaine,
                Please create an expense claim for the below. Relevant details are marked up as requested�
                <expense>
                <cost_centre></cost_centre>
                <total>1024.01</total>
                <payment_method>personal card</payment_method>
                </expense>
                From: Ivan Castle Sent: Friday, 16 February 2018 10:32 AM
                To: Antoine Lloyd <Antoine.Lloyd@example.com>
                Subject: test
                Hi Antoine,
                Please create a reservation at the <vendor>Viaduct Steakhouse</vendor> our
                <description>development team�s project end celebration dinner</description> on <date>Tuesday
                27 April 2017</date>. We expect to arrive around 7.15pm. Approximately 12 people but I�ll
                confirm exact numbers closer to the day.
                Regards,
                Ivan";
        }
        private static string GetInput4()
        {
            return @"Hi Yvaine,
                Please create an expense claim for the below. Relevant details are marked up as requested�
                <expense>
                <cost_centre>DEV002</cost_centre>
                <total>1024.01</total>
                <payment_method>personal card</payment_method>                
                From: Ivan Castle Sent: Friday, 16 February 2018 10:32 AM
                To: Antoine Lloyd <Antoine.Lloyd@example.com>
                Subject: test
                Hi Antoine,
                Please create a reservation at the <vendor>Viaduct Steakhouse</vendor> our
                <description>development team�s project end celebration dinner</description> on <date>Tuesday
                27 April 2017</date>. We expect to arrive around 7.15pm. Approximately 12 people but I�ll
                confirm exact numbers closer to the day.
                Regards,
                Ivan";
        }
        private static string GetInput5()
        {
            return @"Hi Yvaine,
                Please create an expense claim for the below. Relevant details are marked up as requested�
                <expense>
                <cost_centre>DEV002</cost_centre>                
                <payment_method>personal card</payment_method>
                </expense>
                From: Ivan Castle Sent: Friday, 16 February 2018 10:32 AM
                To: Antoine Lloyd <Antoine.Lloyd@example.com>
                Subject: test
                Hi Antoine,
                Please create a reservation at the <vendor>Viaduct Steakhouse</vendor> our
                <description>development team�s project end celebration dinner</description> on <date>Tuesday
                27 April 2017</date>. We expect to arrive around 7.15pm. Approximately 12 people but I�ll
                confirm exact numbers closer to the day.
                Regards,
                Ivan";
        }

        #endregion
    }
}
